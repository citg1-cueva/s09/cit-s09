package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	public String check(){
		return "hello world";
	}

	@GetMapping("/hi")
	public String checkhi(@RequestParam(value="name", defaultValue = "moon")String name){
		return String.format("HI %s", name);
	}
	@GetMapping("/friend")
	public String checkfb(@RequestParam(value="name", defaultValue = "moon")String name, @RequestParam(value="friend", defaultValue = "sun")String friend){
		return String.format("HI %s this is my friend %s", name, friend);
	}

	@GetMapping("/hello/{name}")
	public String hello2(@PathVariable("name") String name){
		return String.format("nice to see you %s", name);
	}

	//ACTIVITY____________________________________---
	ArrayList enrollees = new ArrayList<>();

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="user", defaultValue = "moon")String user){
		enrollees.add(user);
		return String.format("Thank you for enrolling %s!", user);
	}
	@GetMapping("/getEnrollees")
	public ArrayList getEnrollees(){
		return enrollees;
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam(value="name", defaultValue = "none")String name, @RequestParam(value="age", defaultValue = "0") String age){
		return String.format("Hello %s, my age is %s ", name, age);
	}

	@GetMapping("/courses/{courseid}")
	public String course(@PathVariable("courseid")String course){
		String details;
		String id = course.toLowerCase();
		if (id.equals("java101")){
			details = "Name: JAVA 101\nSchedule: MWF 10:00am to 12:00 nn\nPrice: 3000 php ";
		}
		else if (id.equals("sql101")){
			details = "Name: JAVA 101\nSchedule: MWF 8:00am to 10:00 nn\nPrice: 3000 php ";
		}
		else{
			details = "code not found";
		}
		return details;
	}


}
